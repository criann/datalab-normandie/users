# Environnement de traitement

## Terminologie

Afin de bien comprendre la plateforme, nous préférons éclaircir les définitions.

- **groupe** : ensemble d'utilisateurs
- **projet** : ensemble de jobs, d'apps, de variables d'environnement pour l'execution des jobs et d'éléments d'authentification à des registres Docker
- **catalog** : module qui rassemble les dépôts de technologies
- **repository** : dépôt de technologies (logiciels) et leurs différentes versions (**runtime context**) disponibles
- **jobs** : job, à lancement manuel ou selon un planning, permettant de faire tourner une version spécifique d'une technologie
- **job version** : état de la configuration d'un job (commande de lancement, paquet importé, ...)
- **job instance** : lancement d'une version d'un job

## Se connecter

![login](../assets/images/dataops/saagie-login.png)

![platform access](../assets/images/dataops/platform-access.png)

## Vos projets

Des droits (**_Manager_**, **_Editor_** ou seulement **_Viewer_**) vous ont été accordés sur certains projets, ce qui vous permet d'utiliser et gérer vos projets.

Vous pouvez consulter la documentation officielle de l'outil Saagie à cette [URL](https://docs.saagie.io/user/latest/tutorials/projects-module/index.html){: target="_blank"}.

![all projects](../assets/images/dataops/all-projects.png)

![view-jobs](../assets/images/dataops/project-view-jobs.png)

![view-apps](../assets/images/dataops/project-view-apps.png)

!!! warning "Impossible d'envoyer des emails de notification"
    En raison d'un problème de compatibilité que nous espérons pouvoir résoudre dans les prochaines semaines, il est impossible à la plateforme d'envoyer des emails de notifications (changement de l'email de son profil ou notifications de changement d'état d'un job).
    Veuillez nous excuser pour cette gêne opérationnelle que cela pourrait vous provoquer.
