# Processing environment

## Terminology

In order to fully understand the platform, we prefer to clarify the definitions.

- **group**: set of users
- **project**: set of jobs, apps, environment variables for the execution of jobs and authentication elements to Docker registers
- **catalog**: module that gathers technology repositories
- **repository**: repository of software and their different versions (**runtime context**) available
- **jobs**: a job, manually launched or according to a schedule, allowing to run a specific version of a technology
- **job version**: state of the configuration of a job (launch command, imported package, ...)
- **job instance**: launch a version of a job

## Login

![login](../assets/images/dataops/saagie-login.png)

![platform access](../assets/images/dataops/platform-access.png)

## Your projects

You have been granted rights (**_Manager_**, **_Editor_** or only **_Viewer_**) on some projects, which allows you to use and manage your projects.

You can consult the official documentation of the Saagie tool at this [URL](https://docs.saagie.io/user/latest/tutorials/projects-module/index.html){: target="_blank"}.

![all projects](../assets/images/dataops/all-projects.png)

![view-jobs](../assets/images/dataops/project-view-jobs.png)

![view-apps](../assets/images/dataops/project-view-apps.png)

!!! warning "Unable to send notification emails"
    Due to a compatibility issue that we hope to be able to resolve in the coming weeks, it is impossible for the platform to send notification emails (changing one's profile email or job status change notifications).
    We apologize for any operational inconvenience this may cause you.
