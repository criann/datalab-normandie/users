# How to create processing workflows

This guide will allow you to evaluate your strategy to create your jobs within your projects by use cases

## Usecase 1: titanic

- usecase 1a: k-nearest neighbors (k-NN) model in python
    - **Objective**: prediction on the survival of the Titanic passengers
    - **Resources**: <https://www.kaggle.com/c/titanic/overview/>
    - **Technique**: machine learning
    - **Tools used** : scikit-learn, python library minio
- usecase 1b: selective extraction by S3 SELECT
    - **Objective**: retrieve the number of surviving passengers of the Titanic who embarked at Cherbourg
    - **Technique**: S3 SELECT
    - **Tools used** : python library boto3

Guide: [the steps](usecase-01/index.en.md)