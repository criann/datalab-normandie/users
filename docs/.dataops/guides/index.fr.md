# Comment créer des workflows de traitement

Ce guide vous permettra d'évaluer votre stratégie pour créer vos jobs au sein de vos projets par des cas d'utilisation

## Usecase 1: titanic

- usecase 1a: Modèle k-nearest neighbors (k-NN) en python
    - **Objectif** : prévision sur la survie des passagers du Titanic
    - **Resources** : <https://www.kaggle.com/c/titanic/overview/>
    - **Technique** : machine learning
    - **Outils utilisés** : scikit-learn, bibliothèque python minio
- usecase 1b: extraction sélective par S3 SELECT
    - **Objectif** : récupération du nombre de passagers survivants du Titanic qui ont embarqué à Cherbourg
    - **Technique** : S3 SELECT
    - **Outils utilisés** : lib python boto3

Guide : [les étapes](usecase-01/index.fr.md)
