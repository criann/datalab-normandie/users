# Usecase 1: titanic

## Usecase 1a: Modèle k-nearest neighbors (k-NN) en python

- **Objectif** : prévision sur la survie des passagers du Titanic
- **Resources** : <https://www.kaggle.com/c/titanic/overview/>
- **Technique** : machine learning
- **Outils utilisés** : scikit-learn, bibliothèque python minio

La mise en place d'un traitement sur le dataset passe par plusieurs étapes :

1. Récupérer le usecase `titanic` du dépôt Github <https://github.com/criann/datalab-normandie-demos-saagie.git>{: target="_blank"}
2. Déposer le dataset dans le bucket S3 du groupe
3. Créer un Service Account
4. Définir des variables d'environnement au niveau du projet
5. Créer une archive contenant le script de traitement et un fichier `requirements.txt` pour les modules python nécessaires
6. Créer un job de type Python (min. 3.6) et y définir le package (archive créée précédemment) et la ligne de commande d'execution.

### Récupérer une copie du dépôt Github

```bash
git clone https://github.com/criann/datalab-normandie-demos-saagie.git
cd datalab-normandie-demos-saagie/usecases/titanic
```

### Déposer le dataset

1. Connectez-vous sur la console web du datalake S3 : <https://{{ datalake.prefix }}-{{ datalake.console }}.{{ dataops.domain }}> avec votre login et mot de passe
2. Se rendre dans la partie **Buckets**
3. puis cliquer sur le bouton **Browse** de votre bucket de groupe `group-xxxx`
4. Cliquer sur le bouton **New path** et renseigner `titanic`
5. une fois dans le chemin `titanic`, cliquer sur **Upload Files** puis **Upload folder**
6. sélectionner le dossier `data` de votre copie du dépôt Github et valider
7. le dossier `data` a été importé dans le dossier `titanic` de votre bucket de groupe

### Créer un Service Account

1. Créer un Service Account pour le projet
    - **Identity** / **Service Account**
    - bouton **Create Service Account**
    - Valider le formulaire avec les valeurs par défaut
    - Cliquer sur **Download** pour télécharger un fichier contenant un rappel des jetons Access Key et Secret Key générés

### Définir des variables d'environnement au niveau du projet

1. Se connecter sur l'outil de traitement de la donnée avec son login et mot de passe : <https://{{ dataops.prefix }}-{{ dataops.suffix }}.{{ dataops.domain }}>
2. Se rendre dans le projet puis dans la section **Environment variables**
3. Définir des variables pour conserver les valeurs pour l'Access Key et le Secret Key de l'étape précédente
    - Bouton **New variable**
    - Définir les valeurs des champs
    - Bouton **Save**

| Variable name           | Description                            |      is password      | value                                           |
| ----------------------- | -------------------------------------- | :-------------------: | ----------------------------------------------- |
| **DATALAKE_ACCESS_KEY** | Access key pour l'accès au datalake S3 | {{ icons.check_yes }} | `valeur de l'Access Key de l'étape précédente`  |
| **DATALAKE_SECRET_KEY** | Secret key pour l'accès au datalake S3 | {{ icons.check_yes }} | `valeur de la Secret Key de l'étape précédente` |

Des variables sont définies au niveau global et les projets et leurs jobs en héritent automatiquement.

| Variable name       | Description                                      |     is password      | value                             |
| ------------------- | ------------------------------------------------ | :------------------: | --------------------------------- |
| **DATALAKE_HOST**   | FQDN d'accès API au datalake S3                  | {{ icons.check_no }} | `{{ datalake.prefix }}.{{ dataops.domain }}`         |
| **DATALAKE_SCHEME** | HTTP Scheme de l'URL complet (`http` ou `https`) | {{ icons.check_no }} | `https`                           |
| **DATALAKE_URL**    | URL complet d'accès à l'API (https://...)        | {{ icons.check_no }} | `https://{{ datalake.prefix }}.{{ dataops.domain }}` |

### Créer une archive contenant le script de traitement

L'archive qui sera utilisée par le job devra contenir 2 éléments :

- un fichier `requirements.txt` qui sera automatiquement utilisé au lancement du job
- un fichier `__main__.py` qui est le script de lancement du traitement à réaliser

```bash
# depuis le dossier usecases/titanic
cd with_datalake_s3
cp titanic_pandas.py __main__.py
zip archive.zip requirements.txt __main__.py
```

!!! warning "Attention lors de la création de l'archive"
    Lorsque vous créez l'archive, assurez vous bien que les deux fichiers soient à la racine de l'archive.  
    Lorsque vous décompressez l'archive, vous devez directement avoir les fichiers et non un sous-dossier contenant les fichiers.

### Créer un job Python

1. Se connecter sur l'outil de traitement de la donnée avec son login et mot de passe : <https://{{ dataops.prefix }}-{{ dataops.suffix }}.{{ dataops.domain }}>
2. Se rendre dans le projet puis la section **Jobs**
3. Cliquer sur le bouton **New job**
4. Définir un nom de job (par exemple `titanic_s3`)
5. Sélectionner `Extraction / Python`
6. Sélectionner la version de python souhaitée (par défaut `3.9`)
7. Sélectionner l'archive créée dans l'étape précédente comme **Package**
8. Renseigner la **Command line** suivante :

    ```bash linenums="1"
    python {file}
    ```

9. Valider

### Lancer le job

1. se rendre dans le job `titanic_s3` créé dans l'étape précédente depuis la liste des jobs
2. Cliquer sur le bouton **Run** afin de lancer le job dans l'état de configuration du job (la **version**) : cela crée une instance du job pour la dernière version sélectionnée
3. Vous pouvez lancer un rafraîchissement de la page via le symbole :fontawesome-solid-rotate: en haut à droite
    - le statut de l'instance passe par plusieurs états parmi la liste suivante : `Requested`, `Queued`, `Running`, `Failed`, `Killing`, `Killed`, `Succeeded` et `Unknown`.
    - l'objectif est donc d'arriver à un statut `Succeeded`
4. Aller dans la section **Instances** du job : vous y trouverez les informations (état, dates, version du job, logs) concernant les instances du job
    - Les logs sont téléchargeables avec le bouton **Download** et consultables directement

## Usecase 1b: extraction sélective par S3 SELECT

- **Objectif** : récupération du nombre de passagers survivants du Titanic qui ont embarqué à Cherbourg
- **Technique** : S3 SELECT
- **Outils utilisés** : lib python boto3

Les étapes :

- Reprendre les premières étapes du usecase 1
- Se rendre dans le dossier `s3_select`
- Créer l'archive du job
- Créer un job python

### Se rendre dans le dossier `s3_select`

```bash
# depuis le dossier usercases/titanic sur son poste
cd s3_select
```

### Créer l'archive du job

```bash
zip archive.zip requirements.txt __main__.py
```

### Créer un job Python pour le usecase 1b

1. Se connecter sur l'outil de traitement de la donnée avec son login et mot de passe : <https://{{ dataops.prefix }}-{{ dataops.suffix }}.{{ dataops.domain }}>
2. Se rendre dans le projet puis la section **Jobs**
3. Cliquer sur le bouton **New job**
4. Définir un nom de job (par exemple `titanic_s3_select`)
5. Sélectionner `Extraction / Python`
6. Sélectionner la version de python souhaitée (par défaut `3.9`)
7. Sélectionner l'archive créée dans l'étape précédente comme **Package**
8. Renseigner la **Command line** suivante :

    ```bash linenums="1"
    python {file}
    ```

9. Valider
