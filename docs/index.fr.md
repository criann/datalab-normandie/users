---
title: Introduction
hide:
  - toc
---
# Datalab Normandie - Plateforme DataOps

Bienvenue sur la documentation utilisateurs de la plateforme DataOps de [Datalab Normandie](https://www.datalab-normandie.fr){: target="_blank"}.

![datalab logo](assets/images/logo-dln-positif.png#only-light){: loading=lazy class="light"}
![datalab logo](assets/images/logo-dln-negatif.png#only-dark){: loading=lazy class="dark"}

Vous retrouverez ici un ensemble d'informations qui permettent de guider les utilisateurs du service dans leur utilisation de la plateforme.

## Les financeurs

![FEDER](assets/images/LogoUE-couleur.jpg){: loading=lazy style="height: 80px; margin-bottom: 20px;"}
![Région Normandie](assets/images/logo_Normand_CMJN_BAT.jpg){: loading=lazy style="height: 120px"}
