# Mentions légales

## Informations relatives à l'éditeur

Le présent site web est la propriété du Centre Régional Informatique et d'Applications Numériques (CRIANN), association loi 1901, dont le siège est situé 745, avenue de l'Université, 76800 Saint-Etienne du Rouvray.

## Publication

Directeur de publication : Daniel Puechberty, Président du directoire du CRIANN  
Direction chargée de l'édition : CRIANN  
Pour toute question relative au fonctionnement de ce site, vous pouvez contacter l'équipe technique du CRIANN : <dln-support@criann.fr>

## Propriété Intellectuelle

L'ensemble de ce site relève de la législation française et internationale sur le droit d'auteur et la propriété intellectuelle. Des captures d'écran des produits MinIO et Saagie sont incluses dans ce site mais reste la propriété intellectuel des éditeurs de ces logiciels. Tous les droits de reproduction sont réservés, y compris pour les documents téléchargeables et les représentations iconographiques et photographiques. La reproduction de tout ou partie de ce site sur un support électronique quel qu'il soit est formellement interdite sauf autorisation expresse du directeur de la publication.

## Conditions d'utilisation

## CNIL et politique de traitement des données

Conformément à la loi informatique et libertés du 6 janvier 1978 modifiée, vous bénéficiez d'un droit d'accès, de rectification, de limitation, de portabilité et d'opposition aux informations vous concernant.
Si vous souhaitez exercer ces droits, veuillez-vous adresser au Délégué à la protection des données du CRIANN - 745, avenue de l'Université - 76800 Saint-Etienne du Rouvray : <admin@criann.fr>

## Hébergement

Site de documentation (ce site) :

> GitLab Pages  
> 1233 Howard St 2F San Francisco  
> CA 94103 United States of America

Plateforme DataOps et Datalake :

> CRIANN  
> 745, avenue de l'Université  
> 76800 Saint-Etienne du Rouvray  
> France
