# Datalake S3

Le service de Datalake est basé sur le protocole S3 et le projet MinIO.

Un service S3 expose des buckets, ce qui pourrait s'apparenter un peu à des points de partage d'un service de partage de fichiers (SMB, AFP, ...).

Chaque utilisateur du service se voit attribuer un bucket utilisateur et l'accès à un bucket par groupe d'utilisateurs dont il est membre.  

!!! success ""
    Les buckets de groupe sont idéals pour avoir un espace commun où déposer les datasets d'un projet.

!!! warning "Quota sur les buckets"
    Afin de limiter l'occupation qu'un seul utilisateur ou groupe d'utilisateurs pourrait avoir sur l'espace disponible, un système de quota par bucket est appliqué.

    Si vous voyez que votre quota est insuffisant, merci de prendre contact avec <{{ support_email }}> afin d'évaluer un changement de quota pour votre groupe.

L'accès au service se fait par deux canaux :

- pour un usage depuis un programme de calcul ou qui monterait un bucket comme un lecteur réseau sur votre poste, il faut interagir avec l'API S3 dont l'URL est `https://{{ datalake.prefix }}.{{ dataops.domain }}`
- pour un usage de gestion par un humain, une console web est également disponible, ce qui permet par exemple de simplifier l'import et l'export des dataset. L'URL de la console web est <https://{{ datalake.prefix }}-{{ datalake.console }}.{{ dataops.domain }}>

!!! info "Authentification"
    Attention, le protocole S3 utilise une authentification par secret key et access key, des jetons alphanumériques alors que l'authentification à la console web utilise la même authentification que l'outil de traitement de la donnée.

    Vous pouvez générer des jetons d'authentification pour le service S3 depuis la console web du datalake, en créent un Service Account.

## Se connecter via le web

![datalake login](../assets/images/datalake/datalake-login.png)

## Lister ses buckets

![data buckets list](../assets/images/datalake/datalake-buckets-list.png)

## Gérer ses Service accounts

Les Service Account sont des jetons d'authentification S3 qui permettent à vos programmes de s'authentifier auprès du service {{ datalake.prefix }}.

![datalake service-accounts](../assets/images/datalake/datalake-service-accounts.png)

### Créer un Service Account

![datalake create service account](../assets/images/datalake/datalake-create-service-account.png)

Une fois validée, un jeu de jetons vous sera mis à disposition :

![datalake service account created](../assets/images/datalake/datalake-service-account-parameters.png)

### Utiliser une politique d'accès personnalisée

Vous pouvez créer le Service Account avec une politique d'accès plus restrictive que celui du compte utilisateur. Par exemple, un compte utilisateur `user0001` fait partie du groupe `group0001` donc la politique du compte sera la suivante :

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:GetBucketLocation"
      ],
      "Resource": [
        "arn:aws:s3:::*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "arn:aws:s3:::group0001",
        "arn:aws:s3:::group0001/*",
        "arn:aws:s3:::user0001",
        "arn:aws:s3:::user0001/*"
      ]
    },
    {
      "Effect": "Deny",
      "Action": [
        "s3:CreateBucket",
        "s3:DeleteBucket"
      ],
      "Resource": [
        "arn:aws:s3:::*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "arn:aws:s3:::group0001",
        "arn:aws:s3:::group0001/*",
        "arn:aws:s3:::user0001",
        "arn:aws:s3:::user0001/*"
      ],
      "Condition": {
        "StringLike": {
          "s3:prefix": ""
        }
      }
    }
  ]
}
```

mais vous pouvez souhaitez que le Service Account ne donne pas de droit sur le bucket du compte utilisateur car c'est avec ce Service Account que les jobs du projet tournent, les données étant dans le bucket du groupe.  
Dans ce cas, vous souhaiterez refuser l'accès au bucket du compte via une politique d'accès du style :

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Deny",
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "arn:aws:s3:::user0001",
        "arn:aws:s3:::user0001/*"
      ]
    },
    {
      "Effect": "Deny",
      "Action": [
        "s3:ListBucket"
      ],
      "Resource": [
        "arn:aws:s3:::user0001",
        "arn:aws:s3:::user0001/*"
      ],
      "Condition": {
        "StringLike": {
          "s3:prefix": ""
        }
      }
    }
  ]
}
```

La politique d'accès de l'utilisateur se combine avec celle du Service Account, ce qui donnera un accès plus restreint au Service Account.

!!! warning "Limitation sur la taille des politiques"
    Les politiques d'accès déclarées pour les utilisateurs ne peuvent pas dépasser la limite de 20 Ko

Les politiques d'accès définies sont écrits dans la syntaxe JSON supportée par le protocole S3 tel que définie dans la [documentation de Minio](https://docs.min.io/minio/baremetal/security/minio-identity-management/policy-based-access-control.html){: target="_blank"} et selon la [documentation API de référence Amazon S3](https://docs.aws.amazon.com/AmazonS3/latest/API/Welcome.html){: target="_blank"}.
