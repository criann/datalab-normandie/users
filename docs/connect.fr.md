# Comment se connecter

La plateforme mise à disposition est composée de plusieurs éléments :

- un environnement de traitement de la donnée : environnement non renouvelé et arrêté depuis avril 2023
- un datalake S3 pour le stockage des données
- une authentification identique entre l'environnement de traitement de la donnée et la console web du datalake S3

## Les points d'entrée

### Le datalake S3

La plateforme inclut un service S3 pour le stockage des espaces de données des utilisateurs et projets.

Le datalake est basé sur la technologie MinIO, ce qui vous permet de générer des jetons {{ datalake.prefix }}.

- URL de la console Web : <https://{{ datalake.prefix }}-{{ datalake.console }}.{{ dataops.domain }}>{: target="_blank"}
- URL de l'API S3 pour vos programmes : <https://{{ datalake.prefix }}.{{ dataops.domain }}>{: target="_blank"}

Le datalake S3 est composée de 4 machines virtuelles, chacune adressant 4 disques physiques pour une volumétrie totale d'environ 119 To utile.
