---
title: Introduction
hide:
  - toc
---
# Datalab Normandie - DataOps platform

Welcome to the [Datalab Normandie](https://www.datalab-normandie.fr){: target="_blank"} DataOps platform user documentation.

![datalab logo](assets/images/logo-dln-positif.png){: class="light"}
![datalab logo](assets/images/logo-dln-negatif.png){: class="dark"}

You will find here a set of information to guide the users of the service in their use of the platform.

## Funders

![FEDER](assets/images/LogoUE-couleur.jpg){: loading=lazy style="height: 80px; margin-bottom: 20px;"}
![Région Normandie](assets/images/logo_Normand_CMJN_BAT.jpg){: loading=lazy style="height: 120px"}
