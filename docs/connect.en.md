# How to connect

The platform made available is composed of several elements:

- a data processing environment: environment not renewed and stopped in April 2023
- a S3 datalake for data storage
- an identical authentication between the data processing environment and the S3 datalake web console

## The entry points

### The S3 datalake

The platform includes a S3 service for storing user and project data spaces.

The datalake is based on MinIO technology, which allows you to generate S3 tokens.

- Web console URL: <https://{{ datalake.prefix }}-{{ datalake.console }}.{{ dataops.domain }}>{: target="_blank"}
- S3 API URL for your programs: <https://{{ datalake.prefix }}.{{ dataops.domain }}>{: target="_blank"}

The S3 datalake is composed of 4 virtual machines, each addressing 4 physical disks, for a total volume of about 119 TB.
